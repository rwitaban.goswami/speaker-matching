from fastapi import FastAPI
from app.api.speakerapi import SpeakerAPI

app = FastAPI(openapi_url="/api/v1/speaker/openapi.json", docs_url="/api/v1/speaker/docs")

app.include_router(SpeakerAPI, prefix="/api/v1/speaker", tags=["speaker"])