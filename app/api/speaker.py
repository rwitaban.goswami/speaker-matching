import numpy as np
from transformers import AutoFeatureExtractor
from transformers import AutoModelForAudioXVector
import torch
import librosa

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class WavLMPredictionModel:
	def __init__(self) -> None:
		model_name = "microsoft/wavlm-base-plus-sv"
		self.feature_extractor = AutoFeatureExtractor.from_pretrained(model_name)
		self.model = AutoModelForAudioXVector.from_pretrained(model_name).to(device)

	def __call__(self, *args, **kwds):
		assert len(args) >= 1

		feat = self.feature_extractor(args[0], return_tensors="pt", sampling_rate=16000, padding=True).input_values

		with torch.no_grad():
			emb = self.model(feat).embeddings
		return emb

class CosineSimilarity:
	def __init__(self):
		self.sim = torch.nn.CosineSimilarity(dim=-1)

	def __call__(self, *args, **kwds):
		assert len(args) >= 2

		emb1 = torch.nn.functional.normalize(args[0], dim=-1)
		emb2 = torch.nn.functional.normalize(args[1], dim=-1)
		return self.sim(args[0], args[1])

class SpeakerMatchPredictor:
	def __init__(self, *, prediction_model = WavLMPredictionModel(), similarity_model = CosineSimilarity()):
		self.prediction_model = prediction_model
		self.similarity_model = similarity_model

	def sim(self, mp3_1, mp3_2):
		emb1 = self.prediction_model(mp3_1)
		emb2 = self.prediction_model(mp3_2)

		return self.similarity_model(emb1, emb2)

def load_mp3(filename, duration, remove_all_silence, silence_threshold):
	sr = 16000
	audio, sr = librosa.load(filename, sr=sr)
	if remove_all_silence:
		clips = librosa.effects.split(audio, top_db=silence_threshold)
		newAudio = []
		for c in clips:
			data = audio[c[0]: c[1]]
			print(data)
			newAudio.extend(data)
		audio = np.array(newAudio)
	else:
		audio, _ = librosa.effects.trim(audio, top_db=silence_threshold)
	desired_length = int(duration * sr)
	audio = librosa.util.fix_length(audio, size=desired_length)

	return audio
