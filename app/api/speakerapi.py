from typing import List
from app.api.speaker import SpeakerMatchPredictor, WavLMPredictionModel, CosineSimilarity, load_mp3
import torch
import os

from fastapi import Form, File, HTTPException, UploadFile, Request, APIRouter

SpeakerAPI = APIRouter()

@SpeakerAPI.post("/match")
async def submit(file1: UploadFile = File(...), file2: UploadFile = File(...), duration: float = 10.0, remove_all_silence: bool = True, silence_threshold: float = 60.0):
	# Check if audio file
	if not file1.content_type.startswith("audio") or not file2.content_type.startswith("audio"):
		raise HTTPException(status_code=400, detail="Invalid file type, only audio files are allowed")

	# Write audio files to temporary files
	file1_name = file1.filename
	file2_name = file2.filename

	# Create tmp directory if it doesn't exist
	if not os.path.exists("tmp"):
		os.mkdir("tmp")

	file1_path = "tmp/" + file1_name
	file2_path = "tmp/" + file2_name
	with open(file1_path, "wb") as f:
		contents = await file1.read()
		f.write(contents)
	with open(file2_path, "wb") as f:
		contents = await file2.read()
		f.write(contents)

	# Load audio files
	mp3_1 = load_mp3(file1_path, duration=10, remove_all_silence=remove_all_silence, silence_threshold=silence_threshold)
	mp3_2 = load_mp3(file2_path, duration=10, remove_all_silence=remove_all_silence, silence_threshold=silence_threshold)

	# Predict similarity
	predictor = SpeakerMatchPredictor(prediction_model=WavLMPredictionModel(), similarity_model=CosineSimilarity())
	sim = predictor.sim(mp3_1, mp3_2)

	# Delete temporary files
	os.remove(file1_path)
	os.remove(file2_path)
	os.rmdir("tmp")

	return {"similarity": sim.item()}